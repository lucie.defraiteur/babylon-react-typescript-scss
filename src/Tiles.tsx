import * as React from 'react'
import './Tiles.scss';
import Tile from './Tile';

interface Props {
	data: any
}
interface State
{

}

class Tiles extends React.Component<Props, State> {
	render() {
	  return (
		<div className="tiles">
		  {(this.props as any).data.map((data) => {
			return <Tile data={data} key={data.id} />
		  })}
		</div>
	  );
	}
  }
  

 export default Tiles;
